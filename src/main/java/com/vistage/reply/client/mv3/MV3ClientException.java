package com.vistage.reply.client.mv3;

public class MV3ClientException extends Exception {

	private static final long serialVersionUID = -6510467132381851431L;

	public MV3ClientException() {
		super();
	}

	public MV3ClientException(String message, Throwable cause) {
		super(message, cause);
	}

	public MV3ClientException(String message) {
		super(message);
	}

	public MV3ClientException(Throwable cause) {
		super(cause);
	}

}
