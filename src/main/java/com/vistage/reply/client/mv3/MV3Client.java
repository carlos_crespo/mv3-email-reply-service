package com.vistage.reply.client.mv3;

public interface MV3Client {

    <T> T post(String path, Object requestObject, Class<T> responseClass)
            throws MV3ClientException;

}