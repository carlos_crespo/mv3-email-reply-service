package com.vistage.reply.client.mv3;

import java.io.IOException;

import javax.annotation.PostConstruct;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpStatus;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class MV3ClientImpl implements MV3Client {
	
	private static final String URI_TEMPLATE = "%s://%s:%d/%s";
    private static final String APPLICATION_JSON = "application/json";
    private static final String CONTENT_TYPE = "Content-Type";
    private static final String UNEXPECTED_STATUS_CODE_MESSAGE_TEMPLATE = "MV3 returned an unexpected status code: %d";

	@Value("${mv3.protocol}")
	private String protocol;
	@Value("${mv3.host}")
	private String host;
	@Value("${mv3.port}")
	private Integer port;
	@Value("${mv3.username}")
    private String username;
	@Value("${mv3.password}")
    private String password;
	private ObjectMapper objectMapper;
	private CloseableHttpClient httpClient;
	private HttpClientContext httpClientContext;
	
	@PostConstruct
	public void init() {
		final HttpHost httpHost = new HttpHost(host, port, protocol);
		
		final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
		
		final AuthScope authScope = new AuthScope(httpHost.getHostName(), httpHost.getPort());
		final Credentials credentials = new UsernamePasswordCredentials(username, password); 
		credentialsProvider.setCredentials(authScope, credentials);
		
		this.httpClient = HttpClients.custom().setDefaultCredentialsProvider(credentialsProvider).build();
		
		final AuthCache authCache = new BasicAuthCache();
		authCache.put(httpHost, new BasicScheme());
		
		this.httpClientContext = HttpClientContext.create();
		httpClientContext.setAuthCache(authCache);
		
		this.objectMapper = new ObjectMapper();
		objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
	}
	

	
	
	/* (non-Javadoc)
     * @see com.vistage.reply.client.mv3.MV3Client#post(java.lang.String, java.lang.Object, java.lang.Class, java.lang.Object)
     */
	@Override
    public <T> T post(final String path, final Object requestObject, final Class<T> responseClass) 
            throws MV3ClientException {
        CloseableHttpResponse response = null;
        HttpEntity responseEntity = null;
        
        try {
            final HttpPost httpPost = new HttpPost(String.format(URI_TEMPLATE, protocol, host, port, path));
            httpPost.addHeader(CONTENT_TYPE, APPLICATION_JSON);
            
            final String requestBody = objectMapper.writeValueAsString(requestObject);
            
            final HttpEntity requestEntity = new StringEntity(requestBody, "UTF-8");
            httpPost.setEntity(requestEntity);
            
            response = httpClient.execute(httpPost, httpClientContext);
            
            final int statusCode = response.getStatusLine().getStatusCode();
            if(statusCode >= HttpStatus.SC_OK && statusCode <= HttpStatus.SC_MULTI_STATUS) {
                responseEntity = response.getEntity();
                final String responseBody = EntityUtils.toString(responseEntity);
                return objectMapper.readValue(responseBody, responseClass);
            } else {
                throw new MV3ClientException(String.format(UNEXPECTED_STATUS_CODE_MESSAGE_TEMPLATE, statusCode));
            }
        } catch (IOException e) {
            throw new MV3ClientException(e);
        } finally {
            if(responseEntity != null) {
                try {
                    EntityUtils.consume(responseEntity);
                } catch (IOException e) {}
            }
            if(response != null) {
                IOUtils.closeQuietly(response);
            }
        }
    }
}
