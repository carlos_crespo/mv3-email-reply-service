package com.vistage.reply.task;

import java.util.Date;
import java.util.Map;

import javax.mail.Message;
import javax.mail.MessagingException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.vistage.reply.dao.tokeninfo.TokenInfo;
import com.vistage.reply.service.emailreply.EmailReply;
import com.vistage.reply.service.emailreply.EmailReplyException;
import com.vistage.reply.service.emailreply.EmailReplyService;
import com.vistage.reply.service.message.MessageService;
import com.vistage.reply.service.message.MessageServiceException;
import com.vistage.reply.service.mv3reply.MV3Reply;
import com.vistage.reply.service.mv3reply.MV3ReplyResponse;
import com.vistage.reply.service.mv3reply.MV3ReplyService;
import com.vistage.reply.service.mv3reply.MV3ReplyServiceException;
import com.vistage.reply.service.tokeninfo.TokenInfoService;
import com.vistage.reply.service.tokeninfo.TokenInfoServiceException;

import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
public class EmailReplyTask {
	
	@Autowired
	private MessageService messageService;
	@Autowired
	private EmailReplyService emailReplyService;
	@Autowired
	private TokenInfoService tokenInfoService;
	@Autowired
	private MV3ReplyService mv3ReplyService;
	
	@Scheduled(fixedDelayString = "${task.delay:300000}")
	public void processEmails() {
		try {
			log.info("Checking for new messages...");
			final Message[] messages = messageService.getMessages();
			log.info("There are {} new messages", messages.length);

			for(final Message message : messages) {
				String messageId = this.getMessageId(message);
				try {			
				    log.info("Processing message. messageId: [{}]...", messageId);
				    final EmailReply emailReply = emailReplyService.getEmailReply(message);
				    log.info("Message successfully processed. messageId: [{}], emailReĺy: [{}].", messageId, emailReply);
				    
				    final String token = emailReply.getToken();
				    Boolean tokenProcessed = null;

				    final String body = emailReply.getBody();
				    if(StringUtils.isNotBlank(body)) {
				        log.info("Checking if moderation is required. token: [{}]...", token);
				        Boolean moderationRequired = true;
				        try {
				            moderationRequired = tokenInfoService.isUsed(token);
				            log.info("Successfully checked if moderation is required. token: [{}], moderationRequired: [{}].", token, moderationRequired);
				        } catch (TokenInfoServiceException e) {
				            log.error("Exception caught while checking if moderation is required token: [{}], moderationRequired: [{}].", token, e);
				        }
				        
				        final MV3Reply mv3Reply = MV3Reply.builder().emailBody(emailReply.getBody())
				                .emailHeaders(emailReply.getHeaders()).token(token).moderationRequired(moderationRequired).build();
				        
				        log.info("Creating MV3 reply. messageId: [{}], mv3Reply: [{}]...", messageId, mv3Reply); 
				        
				        final MV3ReplyResponse mv3ReplyResponse = mv3ReplyService.createReply(mv3Reply);
				        final Map<String, ?> processed = mv3ReplyResponse.getProcessed();
				        if(processed != null) {
				            tokenProcessed = (Boolean)processed.get(token);				                
				            if(tokenProcessed == null) {
				                tokenProcessed = false;
				                
				                log.warn("mv3ReplyResponse processed map doesn't contain token information. messageId: [{}], mv3Reply: [{}]...", messageId, mv3Reply);
				            }
				            
				        } else {
				            tokenProcessed = false;
				            
				            log.warn("mv3ReplyResponse processed map is null. messageId: [{}], mv3Reply: [{}]...", messageId, mv3Reply);
				        }
				        
				        log.info("MV3 replies succesfully created. messageId: [{}], mv3Reply: [{}], tokenProcessed: [{}].", messageId, mv3Reply, tokenProcessed);				        
				    } else {
				        tokenProcessed = false;
				        
				        log.warn("Reply will be ignored since its body is blank. messageId: [{}], emailReply: [{}].", messageId, emailReply);
				    }
                    
                    
                    final TokenInfo tokenInfo = TokenInfo.builder().from(emailReply.getFrom()).timestamp(new Date())
                            .token(token).processed(tokenProcessed).build();
                    log.info("Storing token info. messageId: [{}], tokenInfo: [{}]...", messageId, tokenInfo); 
                    try {
                        tokenInfoService.save(tokenInfo);
                        log.info("Token info succesfully stored. messageId: [{}], tokenInfo: [{}].", messageId, tokenInfo); 
                    } catch (TokenInfoServiceException e) {
                        log.error("Exception caught while storing token info. messageId: [{}], tokenInfo: [{}].", messageId, tokenInfo, e);
                    }
                    
                    log.info("Moving message. messageId: [{}]...", messageId);
                    try {
                        messageService.moveMessage(message, tokenProcessed);
                        log.info("Message successfully moved. messageId: [{}].", messageId);			        
                    } catch(MessageServiceException e) {
                        log.error("Exception caught while moving message. messageId: [{}]].", messageId, e);
                    }
				} catch (EmailReplyException e) {
				    log.error("Exception caught while processing message. messageId: [{}].", messageId, e);
				} catch (MV3ReplyServiceException e) {
	                log.error("Exception caught while creating MV3 reply. messageId: [{}].", messageId, e);
	            }
			}
		} catch(MessageServiceException e) {
			log.error("Exception caught while checking for new messages.", e);
		}
	}
	
	private String getMessageId(final Message message) {
		String messageId = "N/A";
		try {
			String[] header = message.getHeader("Message-ID");
			if(header != null) {
				messageId = header[0];
			}
		} catch (MessagingException e) {
			log.debug("Unable to get message id.");
		}
		
		return messageId;
	}
}
