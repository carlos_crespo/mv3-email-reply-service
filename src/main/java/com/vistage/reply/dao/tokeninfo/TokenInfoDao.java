package com.vistage.reply.dao.tokeninfo;

public interface TokenInfoDao {

    Boolean isUsed(String token) throws TokenInfoDaoException;

    void save(TokenInfo tokenInfo) throws TokenInfoDaoException;

}