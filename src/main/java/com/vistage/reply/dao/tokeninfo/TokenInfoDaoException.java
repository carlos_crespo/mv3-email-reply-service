package com.vistage.reply.dao.tokeninfo;

public class TokenInfoDaoException extends Exception {

    private static final long serialVersionUID = 68480753663585479L;

    public TokenInfoDaoException() {
        super();
    }

    public TokenInfoDaoException(String message) {
        super(message);
    }

    public TokenInfoDaoException(Throwable cause) {
        super(cause);
    }

    public TokenInfoDaoException(String message, Throwable cause) {
        super(message, cause);
    }
}
