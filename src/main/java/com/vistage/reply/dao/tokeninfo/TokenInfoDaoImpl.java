package com.vistage.reply.dao.tokeninfo;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

@Repository
public class TokenInfoDaoImpl extends JdbcDaoSupport implements TokenInfoDao {
    
    private static final String IS_USED = "select count(*) from TokenInfo where token = ?";
    private static final String SAVE = "insert into TokenInfo values (?, ?, ?,  ?)";
    
    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    @PostConstruct
    public void init() {
         this.setJdbcTemplate(this.jdbcTemplate);
    }
    
    @Override
    public Boolean isUsed(final String token) throws TokenInfoDaoException {
        try {
            return this.getJdbcTemplate().queryForObject(IS_USED, Integer.class, token) > 0;            
        } catch(DataAccessException e) {
            throw new TokenInfoDaoException(e);
        }
    }
    
    @Override
    public void save(final TokenInfo tokenInfo) throws TokenInfoDaoException {
        try {
            this.getJdbcTemplate().update(SAVE, tokenInfo.getToken(), tokenInfo.getFrom(), tokenInfo.getTimestamp(), tokenInfo.getProcessed());            
        } catch(DataAccessException e) {
            throw new TokenInfoDaoException(e);
        }
    }

}
