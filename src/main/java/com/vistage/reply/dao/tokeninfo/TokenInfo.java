package com.vistage.reply.dao.tokeninfo;

import java.util.Date;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class TokenInfo {

    private String token;
    private String from;
    private Date timestamp;
    private Boolean processed;

}
