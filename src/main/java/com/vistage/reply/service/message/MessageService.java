package com.vistage.reply.service.message;

import javax.mail.Message;

public interface MessageService {

	Message[] getMessages() throws MessageServiceException;

	void moveMessage(Message message, Boolean processed) throws MessageServiceException;

}