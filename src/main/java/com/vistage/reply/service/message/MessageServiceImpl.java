package com.vistage.reply.service.message;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.mail.Flags.Flag;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class MessageServiceImpl implements MessageService {
	
    @Value("${imap.host}")
    private String host;
    @Value("${imap.port}")
    private Integer port;
    @Value("${imap.user}")
    private String user;
    @Value("${imap.password}")
    private String password;
    @Value("${imap.ssl:false}")
    private boolean ssl;
    @Value("${imap.folder.from:Inbox}")
    private String from;
    @Value("${imap.folder.to:Inbox/Processed}")
    private String to;
    @Value("${imap.folder.invalid:Inbox/Invalid}")
    private String invalid;
	
	private Session  session;
	private Store store;
	private Map<String, Folder> folders;
	
	@PostConstruct
	public void init() {
		final Properties properties = new Properties();
		properties.setProperty("mail.host", this.host);
		properties.setProperty("mail.port", Integer.toString(this.port));
		properties.setProperty("mail.imap.ssl.enable", Boolean.toString(this.ssl));
		properties.setProperty("mail.transport.protocol", this.ssl ? "imaps" : "imap");
		
		this.session = Session.getInstance(properties);
		
		this.folders = new HashMap<String, Folder>();
	}
	
	@Override
	public Message[] getMessages() throws MessageServiceException {
		try {
			final Folder fromFolder = this.getFolder(this.from);
			
			final Integer count = fromFolder.getMessageCount();
			return fromFolder.getMessages(1, count);
		} catch (MessagingException e) {
			throw new MessageServiceException(e);
		}
	}
	
	@Override
	public void moveMessage(final Message message, final Boolean processed) throws MessageServiceException {
		try {
			final Folder fromFolder = this.getFolder(this.from);
			final Folder toFolder = this.getFolder(processed ? this.to : this.invalid, true);
			
			fromFolder.copyMessages(new Message[] { message }, toFolder);
			
			message.setFlag(Flag.DELETED, true);
			fromFolder.expunge();
		} catch (MessagingException e) {
			throw new MessageServiceException(e);
		}
	}
	
	private Folder getFolder(final String name) throws MessagingException {
		return this.getFolder(name, false);
	}
	
	private Folder getFolder(final String name, final boolean create) throws MessagingException {  
		Folder folder = this.folders.get(name);
		if(folder == null) {
		    final String[] folders = name.split("/");

		    folder = this.getStore().getFolder(folders[0]);
		    for(int i = 1; i < folders.length; i++) {
		        folder = folder.getFolder(folders[i]);
		    }
			
			if(!folder.exists() && create) {
				folder.create(Folder.HOLDS_MESSAGES);
			}
			
			folder.open(Folder.READ_WRITE);
			
			this.folders.put(name, folder);
		} else if(!folder.isOpen()) {
			folder.open(Folder.READ_WRITE);
		}
		
		return folder;
	}
	
	private Store getStore() throws MessagingException {
		if(this.store == null) {
			this.store = this.session.getStore(this.ssl ? "imaps" : "imap");
			this.store.connect(this.user, this.password);
		} else if(!store.isConnected()) {
			this.store.connect(this.user, this.password);
		}
		
		return this.store;
	}

}
