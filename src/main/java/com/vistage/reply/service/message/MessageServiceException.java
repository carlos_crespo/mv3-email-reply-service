package com.vistage.reply.service.message;

public class MessageServiceException extends Exception {

	private static final long serialVersionUID = -3890083246417926712L;

	public MessageServiceException() {
		super();
	}

	public MessageServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	public MessageServiceException(String message) {
		super(message);
	}

	public MessageServiceException(Throwable cause) {
		super(cause);
	}

}
