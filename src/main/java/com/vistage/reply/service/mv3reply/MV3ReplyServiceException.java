package com.vistage.reply.service.mv3reply;

public class MV3ReplyServiceException extends Exception {

    private static final long serialVersionUID = -2903383632122040593L;

    public MV3ReplyServiceException() {
        super();
    }

    public MV3ReplyServiceException(String message) {
        super(message);
    }

    public MV3ReplyServiceException(Throwable cause) {
        super(cause);
    }

    public MV3ReplyServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
