package com.vistage.reply.service.mv3reply;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class MV3Reply {

    private String token;
    @JsonProperty("moderation_required")
    private Boolean moderationRequired;
    @JsonProperty("email_headers")
    private Map<String, String> emailHeaders;
    @JsonProperty("email_body")
    private String emailBody;

}
