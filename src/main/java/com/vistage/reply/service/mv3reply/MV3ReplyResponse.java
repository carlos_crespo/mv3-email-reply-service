package com.vistage.reply.service.mv3reply;

import java.util.Map;

import lombok.Data;

@Data
public class MV3ReplyResponse {
    
    private Map<String, ?> processed;

}
