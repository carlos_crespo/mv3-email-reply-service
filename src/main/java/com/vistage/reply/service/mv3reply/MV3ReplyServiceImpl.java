package com.vistage.reply.service.mv3reply;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vistage.reply.client.mv3.MV3Client;
import com.vistage.reply.client.mv3.MV3ClientException;

@Service
public class MV3ReplyServiceImpl implements MV3ReplyService {
    
    private static final String CREATE_REPLY_URL_TEMPLATE = "wp-json/Vistage/V1/rbe/replies";

    @Autowired
    private MV3Client mv3Client;
    
    /* (non-Javadoc)
     * @see com.vistage.reply.service.mv3reply.MV3ReplyService#createReplies(com.vistage.reply.service.mv3reply.MV3Reply)
     */
    @Override
    public MV3ReplyResponse createReply(final MV3Reply mv3Reply) throws MV3ReplyServiceException {
        try {
            final List<MV3Reply> mv3Replies = new ArrayList<MV3Reply>();
            mv3Replies.add(mv3Reply);
            
            return mv3Client.post(CREATE_REPLY_URL_TEMPLATE, mv3Replies, MV3ReplyResponse.class);
        } catch (MV3ClientException e) {
            throw new MV3ReplyServiceException(e);
        }
    }

}
