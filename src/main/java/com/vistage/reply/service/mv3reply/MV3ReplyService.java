package com.vistage.reply.service.mv3reply;

public interface MV3ReplyService {

    MV3ReplyResponse createReply(MV3Reply mv3Reply) throws MV3ReplyServiceException;

}