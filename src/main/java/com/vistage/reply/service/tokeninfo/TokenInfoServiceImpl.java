package com.vistage.reply.service.tokeninfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vistage.reply.dao.tokeninfo.TokenInfo;
import com.vistage.reply.dao.tokeninfo.TokenInfoDao;
import com.vistage.reply.dao.tokeninfo.TokenInfoDaoException;

@Service
public class TokenInfoServiceImpl implements TokenInfoService {

    @Autowired
    private TokenInfoDao tokenInfoDao;
    
    @Override
    public Boolean isUsed(final String token) throws TokenInfoServiceException {
        try {
            return tokenInfoDao.isUsed(token);
        } catch (TokenInfoDaoException e) {
            throw new TokenInfoServiceException(e);
        }
    }
    
    @Override
    public void save(final TokenInfo tokenInfo) throws TokenInfoServiceException {
        try {
            tokenInfoDao.save(tokenInfo);
        } catch (TokenInfoDaoException e) {
            throw new TokenInfoServiceException(e);
        }
    }

}
