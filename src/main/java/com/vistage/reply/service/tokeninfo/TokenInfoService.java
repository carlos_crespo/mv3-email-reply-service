package com.vistage.reply.service.tokeninfo;

import com.vistage.reply.dao.tokeninfo.TokenInfo;

public interface TokenInfoService {

    Boolean isUsed(String token) throws TokenInfoServiceException;

    void save(TokenInfo tokenInfo) throws TokenInfoServiceException;

}