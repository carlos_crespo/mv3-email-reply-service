package com.vistage.reply.service.tokeninfo;

public class TokenInfoServiceException extends Exception {
    
    private static final long serialVersionUID = -6714463048572895930L;

    public TokenInfoServiceException() {
        super();
    }

    public TokenInfoServiceException(String message) {
        super(message);
    }

    public TokenInfoServiceException(Throwable cause) {
        super(cause);
    }

    public TokenInfoServiceException(String message, Throwable cause) {
        super(message, cause);
    }

}
