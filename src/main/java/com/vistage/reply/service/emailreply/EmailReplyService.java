package com.vistage.reply.service.emailreply;

import javax.mail.Message;

public interface EmailReplyService {

	EmailReply getEmailReply(Message message) throws EmailReplyException;

}