package com.vistage.reply.service.emailreply;

import java.util.Map;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class EmailReply {

    private Map<String, String> headers;
    private String from;
    private String token;
    private String body;

}
