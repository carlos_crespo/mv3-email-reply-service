package com.vistage.reply.service.emailreply;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.BodyPart;
import javax.mail.Header;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class EmailReplyServiceImpl implements EmailReplyService {
	
	private static final Pattern TOKEN_PATTERN = Pattern.compile("(?s)(.*<)?(.*)@.*");
	private static final String CUSTOM_HEADER_PREFIX = "X-";
	
	@Value("${message.orginalMessageRegex:(?m)^[^a-zA-Z0-9]*(?m)$\\R?(?m)^.*myvistage@vistage.com(?s).*}")
	private String originalMessageRegex;
	
	@Override
	public EmailReply getEmailReply(final Message message) throws EmailReplyException {
	    try {
	        final String to = message.getHeader("To")[0];
	        
	        log.info("Extracting token from 'To' header. to: [{}]...", to);
	        
	        try {
	            final String token = this.getToken(to);
	            if(token != null) {
	                log.info("Successfully extracted token from 'To' header. to: [{}], token: [{}].", to, token);
	                
	                log.info("Extracting standard headers...");
	                try {
	                    final Map<String, String> headers = this.getHeaders(message);
	                    
	                    log.info("Successfully extracted standard headers. headers: [{}].", headers);
	                    
	                    log.info("Extracting body...");
	                    
	                    try {
                            final String body = this.getBody(message);
                            
                            log.info("Successfully extracted body. body: [{}].", body);
                            
                            return EmailReply.builder().body(body).headers(headers).token(token)
                                    .from(message.getHeader("From")[0]).build();
	                    } catch(MessagingException | IOException e) {
	                        log.error("Exception caught while trying to extract body.", e);
	                        
	                        throw new EmailReplyException(e);
	                    }
	                } catch(MessagingException e) {
	                    log.error("Exception caught while trying to extract standard headers.", e);
	                    
	                    throw new EmailReplyException(e);
	                }
	            } else {
	                log.error("Unable to extract token from 'To' header. to: [{}].", to);
	                
	                throw new EmailReplyException("Unable to extract token from 'To' header."); 
	            }	            
	        } catch(MessagingException e) {
	            log.error("Exception caught while trying to extract token from 'To' header. to: [{}].", to, e);
	            
	            throw new EmailReplyException(e);
	        }
	    } catch(MessagingException e) {
	        log.error("Exception caught while trying to get message 'To' header.", e);
			
	        throw new EmailReplyException(e);
	    }
	}
	
	private String getToken(String to) throws MessagingException {
	    String token = null;
	    
	    final Matcher matcher = TOKEN_PATTERN.matcher(to);
	    if(matcher.matches()) {
	        token = matcher.group(2);
	    }
	    
	    return token;
	}
	
	private Map<String, String> getHeaders(final Message message) throws MessagingException {
	    final Map<String, String> headers = new HashMap<String, String>();
	    
	    final Enumeration<Header> allHeaders = message.getAllHeaders(); 
	    while(allHeaders.hasMoreElements()) {
	        final Header header = allHeaders.nextElement();
	        final String headerName = header.getName();
	        if(!StringUtils.startsWithIgnoreCase(headerName, CUSTOM_HEADER_PREFIX)) {
	            headers.put(headerName, header.getValue());
	        }
	    }
	    
	    return headers;
	}
	
	private String getBody(final Message message) throws MessagingException, IOException {
	    String body = null;
	       
	    if (message.isMimeType("text/plain")) {
	        body = message.getContent().toString();
	    } else if(message.isMimeType("text/html")) {
	        final String html = message.getContent().toString();
            body = Jsoup.parse(html).text();
	    } else if(message.isMimeType("multipart/*")) {
	        final MimeMultipart mimeMultipart = (MimeMultipart)message.getContent();
	        final int count = mimeMultipart.getCount();
	            
            String htmlContent = null;
            for (int i = 0; i < count; i ++){
                final BodyPart bodyPart = mimeMultipart.getBodyPart(i);
                final String content = bodyPart.getContent().toString();
                
                if (bodyPart.isMimeType("text/plain")) {
                    body = content;
                    break;
                } else if (bodyPart.isMimeType("text/html")) {                  
                    htmlContent = content;
                }
            }
            
            if(body == null && StringUtils.isNotBlank(htmlContent)) {
                body = Jsoup.parse(htmlContent).text();
            }
        }
	    
	    if(body != null && StringUtils.isNotEmpty(this.originalMessageRegex)) {
	        body = body.replaceAll(this.originalMessageRegex, "");	        
	    }
	    
	    return StringUtils.trim(body);
	}	
}
