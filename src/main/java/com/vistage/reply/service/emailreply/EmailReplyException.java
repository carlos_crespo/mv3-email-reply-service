package com.vistage.reply.service.emailreply;

public class EmailReplyException extends Exception {

	private static final long serialVersionUID = 3864422058198193756L;

	public EmailReplyException() {
		super();
	}

	public EmailReplyException(String message) {
		super(message);
	}

	public EmailReplyException(Throwable cause) {
		super(cause);
	}

	public EmailReplyException(String message, Throwable cause) {
		super(message, cause);
	}

}
