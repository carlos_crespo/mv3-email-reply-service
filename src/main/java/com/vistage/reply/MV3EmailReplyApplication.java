package com.vistage.reply;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class MV3EmailReplyApplication {

	public static void main(String[] args) {
		SpringApplication.run(MV3EmailReplyApplication.class, args);
	}
}
